
#ifndef _H_KORE_MYSQL
#define _H_KORE_MYSQL

#include <mysql/my_config.h>
#include <mysql/mysql.h>

#define KORE_MYSQL_FORMAT_TEXT		0
#define KORE_MYSQL_FORMAT_BINARY	1

#if defined(__cplusplus)
extern "C" {
#endif

struct mysql_conn {
    struct kore_event       evt;

	u_int8_t			flags;
	char				*name;
	
    MYSQL *mysql;

	struct mysql_job		*job;
	TAILQ_ENTRY(mysql_conn)		list;
};

struct mysql_db {
    const char *host;
    const char *user;
    const char *name;
    const char *passwd;
    const char *dbname;
    unsigned int port;

    char            *conn_string;

    u_int16_t       conn_max;
    u_int16_t       conn_count;

    const char *unix_socket;
	unsigned long flags;


	LIST_ENTRY(mysql_db)	rlist;
};

struct kore_mysql {
	u_int8_t state;
	int flags;
	char *error;
	MYSQL_RES *result;
	struct mysql_conn *conn;

	struct http_request	*req;

    void (*cb)(struct kore_mysql *, void *);

	LIST_ENTRY(kore_mysql) rlist;
};

extern u_int16_t    mysql_conn_max;

void    kore_mysql_sys_init(void);
void    kore_mysql_init(struct kore_mysql *);
void    kore_mysql_bind_request(struct kore_mysql *, struct http_request *);
void    kore_mysql_bind_callback(struct kore_mysql *, void (*cb)(struct kore_mysql *, void *), void *);
int	    kore_mysql_setup(struct kore_mysql *mysql, const char *dbname);
void	kore_mysql_cleanup(struct kore_mysql *);
int	    kore_mysql_query(struct kore_mysql *, const char *);
int     kore_mysql_get_result(struct kore_mysql *mysql);
int	    kore_mysql_register(const char *, const char *host, const char *dbname, const char *user, const char *passwd, unsigned int port);
void	kore_mysql_logerror(struct kore_mysql *);
void	kore_mysql_queue_remove(struct http_request *);

#if defined(__cplusplus)
}
#endif

#define KORE_MYSQL_STATE_INIT		1
#define KORE_MYSQL_STATE_WAIT		2
#define KORE_MYSQL_STATE_RESULT		3
#define KORE_MYSQL_STATE_ERROR		4
#define KORE_MYSQL_STATE_DONE		5
#define KORE_MYSQL_STATE_COMPLETE	6

#endif

