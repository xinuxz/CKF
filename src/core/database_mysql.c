
#include "../include/ayahesa.h"
#include "tree.h"
#include "kore_mysql.h"

void database_mysql_init(void);

#define variant_mysql_register(n) \
static void n##_mysql_register(const char *); \
void n##_mysql_register(const char *dbname) \
{ \
    char *user = NULL; \
    char *password = NULL; \
    char *host = NULL; \
    unsigned int port = 3306; \
\
    config_get_str(root_app, #n ".user", &user); \
    config_get_str(root_app, #n ".password", &password); \
    config_get_str(root_app, #n ".host", &host); \
\
    kore_mysql_register(#n, host, dbname, user, password, port); \
}

variant_mysql_register(db)
variant_mysql_register(dbr)
variant_mysql_register(dbrw)

void
database_mysql_init(void)
{
    char *dbname = NULL;
    char *dbrwname = NULL;
    char *dbrname = NULL;

    /* Common database */
    config_get_str(root_app, "db.name", &dbname);
    if (dbname) {
        db_mysql_register(dbname);
        return;
    }

    /* Read/write database */
    config_get_str(root_app, "dbrw.name", &dbrwname);
    if (dbrwname) {
        dbrw_mysql_register(dbrwname);
    }

    /* Read database */
    config_get_str(root_app, "dbr.name", &dbrname);
    if (dbrname) {
        dbr_mysql_register(dbrname);
    }
}
