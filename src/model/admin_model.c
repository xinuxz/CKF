/**
 * Copyright (C) 2017 Quenza Inc.
 * All Rights Reserved
 *
 * This file is part of the Ayahesa core.
 *
 * Content can not be copied and/or distributed without the express
 * permission of the author.
 */

#include <ayahesa.h>
#include "kore_mysql.h"

#define T_COST  2
#define M_COST  2^10
#define P_COST  1

struct admin {
    unsigned int id;
    char username;
    char password;
    char phone;
    char real_name;
    char deleted;
    char created_at;
    char updated_at;
};

struct admin admins[30];

int model_admin_auth(const char *, char *);//TODO
int model_admin_update(const char *, char *);

int
model_admin_auth(const char *user, char *secret)
{
    MYSQL_RES *result;
    MYSQL_ROW row;
    MYSQL_FIELD *field;

    struct kore_mysql   mysql;
    unsigned int i;
    int      object_id = 0;
    char                sqlbuffer[256];
    unsigned int num_fields;

    kore_mysql_sys_init();
    kore_mysql_init(&mysql);

    /* Initialize connection */
    if (!kore_mysql_setup(&mysql, "dbrw")) {
        kore_mysql_logerror(&mysql);
        goto done;
    }

    //TODO: prepared statement
    const char selectsql[] =
        "SELECT * "
        "FROM admin "
        "WHERE username='%s' "
        "LIMIT 1";

    /* Execute query */
    snprintf(sqlbuffer, 256, selectsql, user);
    if (!kore_mysql_query(&mysql, sqlbuffer)) {
        kore_mysql_logerror(&mysql);
        goto done;
    }

    result = mysql.result;

    num_fields = mysql_num_fields(mysql.result);

    while (NULL != (field = mysql_fetch_field(result))) {
        printf("field name: %s\n", field->name);
    }

    while (NULL != (row = mysql_fetch_row(result))) {
        unsigned long *lengths;
        lengths = mysql_fetch_lengths(result);

        for (i = 0; i < num_fields; i++) {
            printf("{%.*s} ", (int) lengths[i], (row[i] ? row[i] : "NULL"));
        }

        printf("\n");
    }

done:
    /* All good */
    kore_mysql_cleanup(&mysql);

    return object_id;
}

int
model_admin_update(const char *user, char *secret)
{
    // MYSQL_RES *result;

    // struct kore_mysql   mysql;
    // char                *id;
    // char                *password_hash;
    // char                *password_hash_new;
    
    int                 object_id = 0;

//     const char updatesql[] =
//         "UPDATE users "
//         "SET password='%s' "
//         "WHERE id='%d'";

//     kore_mysql_sys_init();
//     kore_mysql_init(&mysql);

//     /* Initialize connection */
//     if (!kore_mysql_setup(&mysql, "kore")) {
//         kore_mysql_logerror(&mysql);
//         goto done;
//     }

//     //TODO: prepared statement
//     const char selectsql[] =
//         "SELECT * "
//         "FROM admin "
//         "WHERE username='%s' "
//         "LIMIT 1";

//     char                sqlbuffer[256];

//     /* Execute query */
//     snprintf(sqlbuffer, 256, selectsql, user);
//     if (!kore_mysql_query(&mysql, sqlbuffer)) {
//         kore_mysql_logerror(&mysql);
//         goto done;
//     }

//     result = mysql.result;

//     // /* No match */
//     // if (!kore_mysql_ntuples(&mysql))
//     //     goto done;
    
//     /* Fetch password and validate */
//     password_hash = kore_mysql_getvalue(&mysql, 0, 1);
//     if (!crypt_password_verify(password_hash, secret))
//         goto done;

//     // /* Fetch object id */
//     id = kore_mysql_getvalue(&mysql, 0, 0);
//     // object_id = atoi(id);

//     /* Generate new password hash */
//     password_hash_new = crypt_password_hash(secret);

//     // /* Update password hash */
//     // snprintf(sqlbuffer, 256, updatesql, password_hash_new, object_id);
//     // aya_free(password_hash_new);
//     // if (!kore_mysql_query(&mysql, sqlbuffer)) {
//     //     kore_mysql_logerror(&mysql);
//     //     goto done;
//     // }

// done:
//     /* All good */
//     kore_mysql_cleanup(&mysql);

    return object_id;

}
