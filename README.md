# CKF基于C语言Web框架

#### 介绍
[![Build Status](https://travis-ci.org/yorickdewid/ayahesa.svg?branch=master)](https://travis-ci.org/yorickdewid/ayahesa)

高性能、可扩展Web框架。基于MVC模式，适合现在的web特点。这个框架是基于[Kore](https://github.com/jorisvink/kore)的应用框架。代码使用的C99，可以在大部分类Unix平台运行。

#### 软件架构
软件架构说明


#### 安装教程

* 依赖
1. Kore 3.3.0
2. postgresql
3. openssl


#### 功能特色:
* MVC based
* JWT authentication
* JSON-RPC pipeline
* PostgreSQL database connector
* Realtime websockets
* Templating
* File management
* Opque encryption
* IPv6 support
* TLS support


#### 使用说明

* 配置文件
`conf/build.ini` kore编译配置
`conf/ayahesa.ini` kore配置
`conf/framework.ini` 业务相关配置

* 配置说明
```
`src/route.c` 在这里配置路由 `src/controllers/`

`src/middleware.c` 路由和控制器之间的中间件，可以做参数验证、登录验证

`src/trigger.c` 发布订阅模式，事件机制

`src/provider.c` 注入其他服务组件
```

* 编译
```
kodev build
kodev run
```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
